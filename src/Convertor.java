public class Convertor {
	public int char_to_int(char ch){
		int ich = (int) ch;
		if ((ch > 64) && (ch < 91))  ich -= 65;
		if ((ch > 96) && (ch < 123)) ich -= 71;
		return ich;
	}
	public char int_to_char(int ich){
		if ((ich >= 0) && (ich < 26)) ich += 65;
		if ((ich > 25) && (ich < 55)) ich += 71;
		return (char) ich;
	}
}

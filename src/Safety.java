import java.util.*;

public class Safety {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in); 
		
		System.out.println("������� 1 (����� �����������) ��� ������� 2 (����� ������������)");
		int param = 0;
		while (param != 1 && param != 2) 
			param=s.nextInt();
		String inF=null, outF=null;
		//�������� �����
		switch(param){
			case 1: inF  = "to_send.txt";
					outF = "encrypted.txt";
					break;
			case 2: inF  = "encrypted.txt";
					outF = "to_read.txt";
					break;
		}
		// ���������� �����
		getData G = new getData("key.txt");
		char[] Key = G.get();
		
		G = new getData(inF);
		char[] encrypted = G.get();
		
		String message = ""; // ���������� ����������
		int j = 0;			 // ������� ����� �����
		
		Convertor C = new Convertor();	
		char ch = ' ';	// ������� ������
		for (int i = 0; i < encrypted.length; i++){
			ch = encrypted[i];
			if ((ch > 64) && (ch < 91) || (ch > 96) && (ch < 123)){
				int ich = C.char_to_int(Key[j++]) - C.char_to_int(ch);
				if (ich < 0) 
					ich += 52;
				ch = C.int_to_char(ich);
				message += ch;
				if ((j >= Key.length - 1) || (j>=52)) 
					j = 0;
			}
			else
				message += ch;
		}
		setData D = new setData(outF);
		D.set(message);
		
		System.out.println(message);
		s.close();
	}
}

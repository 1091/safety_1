import java.io.*;
public class getData {
	private String fileName;
	public getData(String st){
		fileName=st;
	}
	public char[] get(){
		String line = null;
		String str = "";
		try {
			FileReader fr = new FileReader(fileName);
			BufferedReader in = new BufferedReader(fr);
			while( (line = in.readLine()) != null) {
				str+=line+"\n";
			}
			in.close();
			return str.toCharArray();
		}
		catch(Exception ex) {
			return null;
		}
	}
}
